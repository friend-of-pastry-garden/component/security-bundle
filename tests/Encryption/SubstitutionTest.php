<?php

namespace FOPG\Component\SecurityBundle\Encryption\Tests;

use FOPG\Component\UtilsBundle\Env\Env;
use FOPG\Component\UtilsBundle\Filesystem\File;
use FOPG\Component\UtilsBundle\ShellCommand\ShellCommand;
use FOPG\Component\UtilsBundle\Test\TestCase;
use FOPG\Component\UtilsBundle\Test\TestGiven;
use FOPG\Component\SecurityBundle\Encryption\Substitution;

class SubstitutionTest extends TestCase
{
    const SECTION_HEADER = '[Security:Encrypt:Substitution]';

    public function testSomething(): void
    {
        $this->section(self::SECTION_HEADER.' Manipulation d\'une chaîne de caractères');
        /** @var string $goodPhrase */
        $goodPhrase = "abcdef123456";
        /** @var int $genKeySize */
        $genKeySize = 30;
        /** @var int $shiftRank */
        $shiftRank = 789;
        $this
          ->given(
            description: "Manipulation pour valider le comportement du manager de substitution",
            phrase: $goodPhrase,
            genKeySize: $genKeySize,
            shiftRank: $shiftRank
          )
          ->when(
            description: "Je souhaite effectuer une permutation dans la phrase",
            callback: function(string $phrase, int $genKeySize, ?Substitution &$substitution=null, ?array &$genKey=null) {
              $substitution = new Substitution($phrase);
              $genKey = $substitution->generateKeys($genKeySize);
              $substitution->permute($genKey);
            }
          )
          ->then(
            description: "La permutation doit être effective",
            callback: function(Substitution $substitution, string $phrase) {
              return ((string)$substitution !== $phrase);
            },
            result: true
          )
          ->then(
            description: "La permutation doit pouvoir être réversible",
            callback: function(Substitution $substitution, array $genKey) {
              $substitution->reversePermute($genKey);
              return (string)$substitution;
            },
            result: $goodPhrase
          )
          ->andWhen(
            description: "Je souhaite effectuer un déplacement vers la gauche d'une sous-chaîne",
            callback: function(Substitution $substitution, int $shiftRank) {
              $substitution->shift($shiftRank);
            }
          )
          ->andThen(
            description: "La chaîne doit bien avoir été transformée",
            callback: function(Substitution $substitution, string $phrase) {
              return ((string)$substitution !== $phrase);
            },
            result: true
          )
          ->andThen(
            description: "Je dois pouvoir retrouver la chaîne par application du complémentaire",
            callback: function(Substitution $substitution, int $shiftRank) {
              $substitution->unshift($shiftRank);
              return (string)$substitution;
            },
            result: $goodPhrase
          )
        ;
    }
}
