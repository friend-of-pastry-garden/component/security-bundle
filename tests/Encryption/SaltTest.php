<?php

namespace FOPG\Component\SecurityBundle\Encryption\Tests;

use FOPG\Component\UtilsBundle\Env\Env;
use FOPG\Component\UtilsBundle\Filesystem\File;
use FOPG\Component\UtilsBundle\ShellCommand\ShellCommand;
use FOPG\Component\UtilsBundle\Test\TestCase;
use FOPG\Component\UtilsBundle\Test\TestGiven;
use FOPG\Component\SecurityBundle\Encryption\Salt;

class SaltTest extends TestCase
{
    const SECTION_HEADER = '[Security:Encrypt:Salt]';

    public function testSomething(): void
    {
        $this->section(self::SECTION_HEADER.' Manipulation du SALT aléatoire');

        $minLength=8;
        $maxLength=13;
        $content = "eabcd";
        $goodPhrase = " abcd";

        $this
          ->given(
            description: "Manipulation pour valider le comportement du SALT",
            minLength: $minLength,
            maxLength: $maxLength,
            content: $content,
            goodPhrase: $goodPhrase
          )
          ->when(
            description: "Je souhaite valider la génération d'un SALT aléatoire",
            callback: function(int $minLength, int $maxLength, ?Salt &$salt=null, ?string &$generatedSalt=null) {
              $salt = new Salt($minLength, $maxLength);
              $salt
                ->addRank("abcd")
                ->addRank("bcda")
                ->addRank("ca")
                ->addRank("db")
              ;
              $generatedSalt = $salt->generate();
            }
          )
          ->then(
            description: "Le SALT doit être retrouvé au début d'une phrase (le premier caractère de la phrase n'appartient pas aux rangs)",
            callback: function(Salt $salt, string $generatedSalt, string $content) {
              $phrase = $generatedSalt.$content;
              return ($salt->checkLeft($phrase)>0);
            },
            result: true
          )
          ->then(
            description: "L'absence de SALT doit être détecté",
            callback: function(Salt $salt, string $generatedSalt, string $content, int $minLength) {
              /** @var string $badSalt Le salt est corrompu car incomplet */
              $badSalt = mb_substr($generatedSalt,0,$minLength-1);
              $badPhrase = $badSalt.$content;
              return $salt->checkLeft($badPhrase);
            },
            result: -1,
            onFail: function(TestGiven $whoami) { $whoami->addError("Erreur inattendue", 101); }
          )
          ->andThen(
            description: "Le SALT doit pouvoir être extrait d'une phrase",
            callback: function(Salt $salt, string $generatedSalt, string $content, ?string &$result=null) {
              $phrase = $generatedSalt.$content;
              $result = $salt->extract($phrase);
              return ($result === $content);
            },
            result: true
          )
          ->andWhen(
            description: "Lorsque que je souhaite ajouter un SALT avec sécurisation à l'extraction",
            callback: function(int $minLength, int $maxLength, string $goodPhrase, ?Salt &$newSalt=null, ?string &$completePhrase=null) {
              $newSalt = new Salt($minLength, $maxLength);
              $newSalt
                ->addRank("az ")
                ->addRank("bp&")
              ;
              $completePhrase = Salt::decorate($newSalt, $goodPhrase, strategy: Salt::DECORATE_ON_LEFT);
            }
          )
          ->andThen(
            description: "Le SALT de gauche doit pouvoir être extrait d'une phrase de manière certaine même si le premier caractère est à l'intérieur des rangs",
            callback: function(Salt $newSalt, string $completePhrase) {
              return $newSalt->extract($completePhrase);
            },
            result: $goodPhrase
          )
        ;

        $this
          ->given(
            description: "Contrôle de la lecture à droite du SALT",
            minLength: $minLength,
            maxLength: $maxLength,
            goodPhrase: $goodPhrase
          )
          ->when(
            description: "Je souhaite ajouter un SALT à droite d'une phrase (plantage sur des cas rares [ex: 'aba' et rang unique ['a']] - géré par le 'decorate')",
            callback: function(int $minLength, int $maxLength, ?Salt &$mySalt=null, ?string &$generatedSalt=null) {
              $mySalt = new Salt($minLength, $maxLength);
              $mySalt
                ->addRank("a& ")
                ->addRank('bue')
                ->addRank('cfw')
                ->addRank('kp')
              ;
              $generatedSalt = $mySalt->generate();
            }
          )
          ->then(
            description: "Je dois retrouver l'index de césure à droite du SALT",
            callback: function(Salt $mySalt, string $goodPhrase, string $generatedSalt) {
              $phraseWithSalt = $goodPhrase.$generatedSalt;
              $index = $mySalt->checkRight($phraseWithSalt);
              return $index;
            },
            result: count(mb_str_split($goodPhrase))
          )

          ->andThen(
            description: "Le SALT doit pouvoir être extrait d'une phrase",
            callback: function(Salt $mySalt, string $generatedSalt, string $goodPhrase) {
              $phraseWithSalt = $goodPhrase.$generatedSalt;
              $result = $mySalt->extract($phraseWithSalt);
              return $result;
            },
            result: $goodPhrase
          )
          ->andWhen(
            description: "Lorsque que je souhaite ajouter un SALT à droite avec sécurisation à l'extraction",
            callback: function(int $minLength, int $maxLength, string $goodPhrase, ?Salt &$newSalt=null, ?string &$completePhrase=null) {
              $newSalt = new Salt($minLength, $maxLength);
              $newSalt
                ->addRank("az ")
                ->addRank("bp&")
                ->addRank("uv")
                ->addRank("cd")
              ;
              $completePhrase = Salt::decorate($newSalt, $goodPhrase, strategy: Salt::DECORATE_ON_RIGHT);
            }
          )
          ->andThen(
            description: "Le SALT doit pouvoir être extrait d'une phrase de manière certaine même si le premier caractère est à l'intérieur des rangs",
            callback: function(Salt $newSalt, string $completePhrase) {
              return $newSalt->extract($completePhrase);
            },
            result: $goodPhrase
          )
          ->andWhen(
            description: "Lorsque que je souhaite ajouter un SALT à droite & à gauche avec sécurisation à l'extraction",
            callback: function(int $minLength, int $maxLength, string $goodPhrase, ?Salt &$newSalt=null, ?string &$completePhrase=null) {
              $newSalt = new Salt($minLength, $maxLength);
              $newSalt
                ->addRank("az ")
                ->addRank("bp&")
                ->addRank("uv")
                ->addRank("cd")
              ;
              $completePhrase = Salt::decorate($newSalt, $goodPhrase, strategy: Salt::DECORATE_BOTH);
            }
          )
          ->andThen(
            description: "Le SALT doit pouvoir être extrait d'une phrase de manière certaine après nettoyage à droite et à gauche",
            callback: function(Salt $newSalt, string $completePhrase) {
              return $newSalt->extract($completePhrase);
            },
            result: $goodPhrase
          )
          ->andWhen(
            description: "Lorsque que je souhaite ajouter un SALT à droite & à gauche avec sécurisation à l'extraction en situation extrême",
            callback: function(int $minLength, int $maxLength, ?Salt &$newSalt=null, ?string &$completePhrase=null) {
              $newSalt = new Salt($minLength, $maxLength);
              $newSalt
                ->addRank("a")
              ;
              $completePhrase = Salt::decorate($newSalt, "a", strategy: Salt::DECORATE_BOTH);
            }
          )
          ->andThen(
            description: "Le SALT doit pouvoir être extrait d'une phrase de manière certaine après nettoyage à droite et à gauche",
            callback: function(Salt $newSalt, string $completePhrase) {
              return $newSalt->extract($completePhrase);
            },
            result: "a"
          )
        ;
    }
}
