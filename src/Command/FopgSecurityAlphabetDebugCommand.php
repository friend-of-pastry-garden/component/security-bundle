<?php

namespace FOPG\Component\SecurityBundle\Command;

use FOPG\Component\UtilsBundle\Collection\Collection;
use FOPG\Component\UtilsBundle\Command\AbstractCommand;
use FOPG\Component\UtilsBundle\Filesystem\File;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

#[AsCommand(
    name: 'fopg:security:alphabet-debug',
    description: 'Détection de l\'alphabet de référence pour l\'application',
)]
class FopgSecurityAlphabetDebugCommand extends AbstractCommand
{
    public function __construct(ContainerInterface $container) {
        parent::__construct($container);
    }

    protected function configure(): void
    {
        parent::configure();
        $this
            ->addArgument('folder', InputArgument::REQUIRED, 'répertoire contenant l\'ensemble des fichiers texte devant servir à l\'apprentissage')
            ->addArgument('segments', InputArgument::REQUIRED, 'nombre de segments pour répartir équitablement l\'alphabet')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        parent::execute($input, $output);
        /** @var string $folder */
        $folder = $input->getArgument('folder',null);
        /** @var int $segments */
        $segments = (int)$input->getArgument('segments',1);
        /** @var array<string, float> $frequencies */
        $frequencies = File::get_alphabet($folder, $segments);
        $rows=[];
        foreach($frequencies as $index => $frequency) {
          $rows[]=[$index+1, str_replace(["\n","\r","\t"],['\n','\r','\t'], '['.implode("][",$frequency['alphabet']).']'), number_format($frequency['rate']*100,2).' %'];
        }
        $table = new Table($output);
        $table->setHeaders(
          ['section', 'éléments', 'fréquence'],
        );
        $table->setRows($rows);
        $table->render();
        $this->unlock();
        return Command::SUCCESS;
    }
}
