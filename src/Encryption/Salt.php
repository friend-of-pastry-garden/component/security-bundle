<?php

namespace FOPG\Component\SecurityBundle\Encryption;

class Salt {
  const DECORATE_ON_LEFT = 1;
  const DECORATE_ON_RIGHT= 2;
  const DECORATE_BOTH    = 3;

  /** @var array Tableau des termes autorisés par rang */
  private array $_ranks=[];
  /** @var int Longueur minimale du salt aléatoire */
  private int $_minLength=0;
  /** @var int Longueur maximale du salt aléatoire */
  private int $_maxLength=0;

  public function __construct(int $minLength, int $maxLength=0) {
    $this->_minLength = $minLength;
    $this->_maxLength = $maxLength;
    if($maxLength < $minLength)
      $this->_maxLength = $minLength;
  }

  public function setMaxLength(int $maxLength): self {
    $this->_maxLength = $maxLength;
    return $this;
  }

  public function getMaxLength(): int {
    return $this->_maxLength;
  }

  public function setMinLength(int $minLength): self {
    $this->_minLength = $minLength;
    return $this;
  }

  public function getMinLength(): int {
    return $this->_minLength;
  }

  /**
   * Ajout d'un rang pour la génération du SALT
   *
   * @param string $accreditedWords Liste de lettres autorisés pour la génération du SALT
   * @return self
   */
  public function addRank(string $accreditedWords): self {
    $this->_ranks[]=mb_str_split($accreditedWords);
    return $this;
  }

  /**
   * Génération d'un SALT
   *
   * Renvoi de null si aucun rang n'a été défini
   *
   * @return ?string
   */
  public function generate(): ?string {

    if(0 === count($this->_ranks))
      return null;

    $output="";
    /** @var int $length */
    $length = rand($this->_minLength, $this->_maxLength);
    for($i=0;$i<$length;$i++) {
      /** @var int $posRank */
      $posRank = $i % count($this->_ranks);
      /** @var int $index */
      $index = rand(0,count($this->_ranks[$posRank])-1);
      $output.=$this->_ranks[$posRank][$index];
    }
    return $output;
  }

  /**
   * Fonction de contrôle de conformité du SALT avec renvoi de l'index de césure
   *
   * La méthode renvoit -1 si le SALT n'a pu être identifié
   *
   * @var string $phrase
   * @return int
   */
  public function checkLeft(string $phrase): int {

    if(0 === count($this->_ranks))
      return false;

    /** @var array $letters Tableau des lettres de la phrase */
    $letters = mb_str_split($phrase);
    /** @var int $minLength */
    $minLength = $this->_minLength;
    /** @var bool $output */
    $output = true;
    /** @var int $i */
    $i=0;
    for(;$i<$this->_maxLength;$i++) {
      /** @var int $posRank */
      $posRank = $i % count($this->_ranks);
      /** @var bool $localCheck */
      $localCheck = isset($letters[$i]) && in_array($letters[$i], $this->_ranks[$posRank]);
      if(false === $localCheck)
        return ($i >= $minLength) ? $i : -1;
    }

    return ($this->_maxLength === $i) ? $i : -1;
  }

  /**
   * Fonction de contrôle de conformité du SALT avec renvoi de l'index de césure
   *
   * La méthode renvoit -1 si le SALT n'a pu être identifié
   *
   * @var string $phrase
   * @return int
   */
  public function checkRight(string $phrase): int {
    /** @var array $reverseLetters Tableau des lettres inversées de la phrase */
    $reverseLetters = array_reverse(mb_str_split($phrase));
    /** @var int $length */
    $length = count($reverseLetters);
    /** @var int $minLength */
    $minLength = $this->_minLength;
    /** @var int $nbRanks */
    $nbRanks = count($this->_ranks);
    /** @var bool $output */
    $output = true;
    /** @var int $j */
    $j=0;
    $localMaxLength=$length;
    for($j=0; $j<$nbRanks;$j++) {
      /** @var int $i */
      $i=0;
      for(;$i<$this->_maxLength;$i++) {
        /** @var int $posRank */
        $posRank = abs($nbRanks - (($i+$j) % $nbRanks) - 1);
        /** @var bool $localCheck */
        $localCheck = isset($reverseLetters[$i]) && in_array($reverseLetters[$i], $this->_ranks[$posRank]);
        if(false === $localCheck) {
          if( ($i>=$minLength) && ( ($length-$i) < $localMaxLength) )
            $localMaxLength=$length-$i;
          break;
        }
      }

      if($this->_maxLength === $i)
        return ($length-$i);

    }

    return ($localMaxLength < $length) ? $localMaxLength : -1;
  }

  /**
   * Fonction d'extraction du SALT
   *
   * La méthode renvoit null si le SALT n'a pu être identifié
   *
   * @var string $phrase
   * @return ?string
   */
  private function _extractLeft(string $phrase): ?string {
    /** @var int $pos */
    $pos = $this->checkLeft($phrase);
    if($pos>0)
      return mb_substr($phrase, $pos);
    return null;
  }

  private function _extractRight(string $phrase): ?string {
    /** @var int $pos */
    $pos = $this->checkRight($phrase);
    if($pos>0)
      return mb_substr($phrase,0, $pos);
    return null;
  }

  private function _extractBoth(string $phrase): ?string {
    /** @var ?string $tmp */
    $tmp = $this->_extractLeft($phrase);
    return (null !== $tmp) ? $this->_extractRight($tmp) : null;
  }

  public function extract(string $phrase): ?string {
    $tmp = $this->_extractBoth($phrase);
    if(null !== $tmp)
      return $tmp;
    $tmp = $this->_extractRight($phrase);
    if(null !== $tmp)
      return $tmp;
    return $this->_extractLeft($phrase);
  }

  private static function _decorateOnLeft(Salt $salt, string $phrase, int $minLength, int $maxLength): ?string {
    /** @var int $i */
    for($i=$minLength;$i<=$maxLength;$i++) {
      $salt->setMinLength($i);
      /** @var ?string $generatedSalt */
      $generatedSalt = $salt->generate();
      /** @var string $candidateNewPhrase */
      $candidateNewPhrase = $generatedSalt.$phrase;
      if($salt->_extractLeft($candidateNewPhrase) === $phrase)
        return $candidateNewPhrase;
    }
    return null;
  }

  private static function _decorateOnRight(Salt $salt, string $phrase, int $minLength, int $maxLength): ?string {
    /** @var int $i */
    for($i=$minLength;$i<=$maxLength;$i++) {
      $salt->setMinLength($i);
      /** @var ?string $generatedSalt */
      $generatedSalt = $salt->generate();
      /** @var string $candidateNewPhrase */
      $candidateNewPhrase = $phrase.$generatedSalt;
      if($salt->_extractRight($candidateNewPhrase) === $phrase)
        return $candidateNewPhrase;
    }
    return null;
  }

  public static function decorate(Salt $salt, string $phrase, string $strategy=self::DECORATE_BOTH): ?string {
    $minLength = $salt->getMinLength();
    $maxLength = $salt->getMaxLength();
    $copySalt  = clone $salt;
    $newPhrase = null;
    switch($strategy) {
      case self::DECORATE_ON_LEFT:
        $newPhrase = self::_decorateOnLeft($copySalt, $phrase, $minLength, $maxLength);
        break;
      case self::DECORATE_ON_RIGHT:
        $newPhrase = self::_decorateOnRight($copySalt, $phrase, $minLength, $maxLength);
        break;
      case self::DECORATE_BOTH:
        $tmpPhrase = self::_decorateOnLeft($copySalt, $phrase, $minLength, $maxLength);
        $newPhrase = self::_decorateOnRight($copySalt, $tmpPhrase, $minLength, $maxLength);
    }
    return $newPhrase;
  }
}
