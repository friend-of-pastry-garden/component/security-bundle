<?php

namespace FOPG\Component\SecurityBundle\Encryption;

class Substitution {

  private array $_letters=[];

  public function __construct(string $phrase) {
    $this->_letters = mb_str_split($phrase);
  }

  public function __toString() {
    return implode("", $this->_letters);
  }

  /**
  private ?string $_alphabet=null;

  public function setAlphabet(string $alphabet): self {
    $this->_alphabet = $alphabet;
    return $this;
  }

  public function getAlphabet(): ?string {
    return $this->_alphabet;
  }
  */

  /**
   * Génération d'un ensemble de clés de substitution aléatoire
   *
   * @var int $size
   * @return array
   */
  public function generateKeys(int $size): array {
    $min = 0;
    $max = count($this->_letters)-1;
    $output=[];
    for($i=0;$i<$size;$i++)
      $output[]=rand($min,$max);
    return $output;
  }

  /**
   * Gestion des permutations multiples
   *
   * Soit k le nombre de rangs
   * Compléxité en temps : O(k)
   *
   * @var array<int> $ranks
   * @return self
   */
  public function permute(array $ranks): self {
    /** @var int $rank */
    foreach($ranks as $rank)
      if(is_int($rank))
        $this->_singlePermute($rank);
    return $this;
  }

  /**
   * Gestion de l'annulation des permutations mutliples
   *
   * Soit k le nombre de rangs
   * Compléxité en temps : O(k)
   *
   * @var array<int> $ranks
   * @return self
   */
  public function reversePermute(array $ranks): self {
    $reverseRanks = array_reverse($ranks);
    return $this->permute($reverseRanks);
  }

  /**
   * Permutation entre deux caractères à l'intérieur d'une phrase
   *
   * Compléxité en temps : O(1)
   *
   * @param int $rank
   * @return self
   */
  private function _singlePermute(int $rank): self {
    /** @var int $cpt */
    $cpt = count($this->_letters);
    if(0 === $cpt)
      return $this;

    $realRank = $rank % $cpt;
    $tmp = $this->_letters[$realRank];
    $this->_letters[$realRank] = $this->_letters[0];
    $this->_letters[0] = $tmp;

    return $this;
  }

  public function unshift(int $rank): self {
    /** @var int $cpt */
    $cpt = count($this->_letters);
    if(0 === $cpt)
      return $this;
    /** @var int $realRank */
    $realRank = $rank % $cpt;
    /** @var int $complementary */
    $complementary = $cpt - $realRank;
    $this->shift($complementary);
    return $this;
  }

  public function shift(int $rank): self {
    /** @var int $cpt */
    $cpt = count($this->_letters);
    if(0 === $cpt)
      return $this;

    $realRank = $rank % $cpt;
    $lftCpt = $realRank;
    $rgtCpt = $cpt-$realRank;

    $tmp = implode("", $this->_letters);
    $lft = mb_substr($tmp,0, $lftCpt);
    $rgt = mb_substr($tmp,$lftCpt);
    $tmp = $rgt.$lft;
    $this->_letters = mb_str_split($tmp);
    return $this;
  }
}
